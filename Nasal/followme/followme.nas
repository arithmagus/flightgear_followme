###############################################################################
##
##  Animated FollowMe Car
##
##  Copyright (C) 2011  Andre Dietrich
##  This file is licensed under the GPL license version 2 or later.
##
###############################################################################

  print("loading FollowMe version 0.8.3");

###############################################################################

#io.load_nasal(getprop("/sim/fg-root") ~ "/Nasal/followme/followme.nas", "followme");

#
# HELPER FUNCTIONS
# -----------------
  var FOLLOWMEmodel = "Models/Airport/followme/follow_me.xml";

# ***** parsing airport xml files *****

      var normdeg = func(a) {
  while (a >= 180)
    a -= 360;
  while (a < -180)
    a += 360;
  return a;
      }

# ***** end of parsing airport xml files *****

      var runwayinfo = func(apt) {
        var max = 361;
  var rwy = nil;
        var dir = getprop("/orientation/heading-deg");
      
        foreach (var r; keys(apt.runways)) {
          var curr = apt.runways[r];
          var deviation = math.abs(normdeg(dir - curr.heading)) + 1e-20;

          if ((deviation < max) and (r[0] != `H`)) {
            max = deviation;
            rwy = curr;
            rwy.name = r;
          }
        }
        return rwy;
      }
      

      var running = 1;
      var actdistance = 0;
      var olddistance = 0;
      
# ***** Airport class definition *****

var Airport = {};

Airport.new = func() {
  var m = { parents: [Airport] };
    m.nodes = {};
    m.parkings = {};
    m.coord = geo.Coord.new();
    m.loadedApt = "";

    m.load();
    return m;
  }
        
# reload next Airport if needed
Airport.load = func() {
  var object = me;

  var genCoord = func(text) {
     var tmp = split(" ", text);
     var sign = "";
     var coord = tmp[0];
     if (coord[0] == `N`) {
       sign = "+"; }
     if (coord[0] == `E`)
       sign = "+";
     if (coord[0] == `S`)
       sign = "-";
     if (coord[0] == `W`)
       sign = "-";
     coord[0] = `0`;
              
     return sign ~ (coord + (tmp[1] / 60));
  }

  var tagopen = func(name, attr) {      
    var id = 0;
    var from = 0;
    var to = 0;
    var temp = 0;
    if (name == "Parking") {
      lat = genCoord(attr["lat"]);
      lon = genCoord(attr["lon"]);
      print("Parking: ",lat, "/", lon);
      if ((id = num(attr["index"])) != nil) {
        object.nodes[id] = parking.new(genCoord(attr["lat"]), genCoord(attr["lon"]), attr["heading"], attr["radius"], attr["type"], attr["name"], attr["number"], id);
        
        #store parkings in sorted array
        temp = size(object.parkings);
        #print ("parkings size: ", temp);
        while ((temp > 0) and (object.parkings[temp-1].radius > attr["radius"])) {              
          object.parkings[temp] = object.parkings[temp-1];
          temp = temp -1;
        }
        object.parkings[temp] = object.nodes[id];
      } else {
        print("cannot parse numeric attribute from parking ", attr["index"]);
      }        
    }                
    if (name == "node") {
      lat = genCoord(attr["lat"]);
      lon = genCoord(attr["lon"]);
      print("Point: ",lat, "/", lon);
      if ((id = num(attr["index"])) != nil) {
        object.nodes[id] = node.new(genCoord(attr["lat"]), genCoord(attr["lon"]), id);
      } else {
        print("cannot parse numeric attribute from point ", attr["index"]);
      }
    }
    if (name == "arc") {
      if (((from = num(attr["begin"])) != nil) and ((to = num(attr["end"])) != nil)) {
        if (contains(object.nodes, from) and contains(object.nodes, to)) {
          object.nodes[from].addsucc(to);
        } else {
          print("node ", from, " or ", to, " not listed.");
        }            
      } else {
        print("cannot parse start or end attribute from arc ", attr["begin"], "->", attr["end"]);
      }
    }
  }
  var tagclose = func(name) { }

  var actairport = airportinfo();
  if (me.loadedApt != actairport.name) {
    print("new airport: " ~ actairport.name ~ ", loading xml file...");
    me.nodes = {};
    me.parkings = {};
    me.coord.set_latlon(actairport.lat, actairport.lon);
    var airportfolder = "/Scenery/Airports/" ~ chr(actairport.id[0]) ~ "/" ~ chr(actairport.id[1]) ~ "/" ~ chr(actairport.id[2]);
    parsexml(getprop("/sim/fg-root") ~ airportfolder ~ "/" ~ actairport.id ~ ".groundnet.xml", tagopen, tagclose, func(data) { }, func(target, data) { } );
    print("Airport has ", size(me.nodes), " nodes and ", size(me.parkings), " parkings.");
    me.loadedApt = actairport.name
  } else {
    print("same airport, no new data loaded.");
  }
}

# return if valid data has been loaded.
Airport.dataIsValid = func() {
  print("num parkings:", size(me.parkings));
  return (size(me.parkings) > 0);
}

# return best parking
# todo: size of aircraft
Airport.get_best_parking = func() {
  for (var a = 0; a < size(me.parkings); a += 1) {
    if ((me.parkings[a].radius < 34) or (me.parkings[a].isBusy() == 1)) {
      continue;
    }
    print("parking no ", a, " called ", me.parkings[a].name ~ me.parkings[a].number, " has size ", me.parkings[a].radius, " and index ", me.parkings[a].index);
    return me.parkings[a].index;
  }
  return -1;
};

# return next holding point for followme car
# todo: estimate better braking scheme 
Airport.get_next_exit = func() {
  var planePos = geo.Coord.new();
  var speedkmh = getprop("/velocities/groundspeed-kt")  * 1.852;
  planePos.set_latlon(getprop("/position/latitude-deg"), getprop("/position/longitude-deg"), getprop("/position/altitude-ft") * 0.3048);
  planePos = planePos.apply_course_distance(getprop("/orientation/heading-deg"), speedkmh * speedkmh / 100);
  var next = -1;
  var shortest = 0;
  var distance = 0;
  foreach (var n; keys(me.nodes)) {
    distance = planePos.direct_distance_to(me.nodes[n].coord);
    if ((next == -1) or (shortest > distance)) {
      next = n;
      shortest = distance;
    }   
  }
  return next;
};

       
# ***** end of Airport class definition *****

# ***** followme class definition *****

var followme_msg = func setprop("sim/messages/ai-plane", call(sprintf, arg));
var ground_msg = func setprop("sim/messages/ground", call(sprintf, arg));


var FollowMe = {};

FollowMe.new = func(path) {
        var m = { parents: [FollowMe] };
        m.airport = Airport.new();
        m.speed = 80 / 3.6;
        m.heading = 0;
        m.headingtarget = 0;
        m.headingdiff = 0;
        m.alt = geodinfo(m.airport.coord.lat(), m.airport.coord.lon())[0] * M2FT;
        m.coord = geo.Coord.new(m.airport.coord);
        m.nextwpt = nil;
        m.dt = 0;
        m.distance = 0;
        m.interval = 5;

        m.openlist = {}; 
        m.closedlist = {};
        m.f_value = {};
        m.g_value = {};
        
        var tmpnode = props.globals.getNode("models", 1);
        for (var num = 0; 1; num += 1)
          if (tmpnode.getChild("model", num, 0) == nil)
            break;
        m.model = tmpnode.getChild("model", num, 1);

        var tmpnode = props.globals.getNode("ai/models", 1);
        for (var num = 0; 1; num += 1)
          if (tmpnode.getChild("followme", num, 0) == nil)
            break;
        m.ai = tmpnode.getChild("followme", num, 1);
        
        var id_used = {};
  foreach (var t; props.globals.getNode("ai/models", 1).getChildren()) {
          if ((var c = t.getNode("id")) != nil)
      id_used[c.getValue()] = 1;
  }
  for (var aiid = -2; aiid; aiid -= 1)
    if (!id_used[aiid])
      break;
      
  var flash_left = aircraft.light.new(m.ai.getPath() ~ "/flash_left", [0.1, 0.05, 0.05, 0.45]);
  flash_left.switch(1);
  var flash_right = aircraft.light.new(m.ai.getPath() ~ "/flash_right", [0.05, 0.05, 0.05, 0.5]);
  flash_right.switch(1);
      
        m.callsign = airportinfo().id ~ "FM" ~ aiid;
        m.ai.getNode("id", 1).setIntValue(aiid);
        m.ai.getNode("callsign", 1).setValue(m.callsign ~ "");
        m.ai.getNode("valid", 1).setBoolValue(1);

        m.latN = m.ai.getNode("position/latitude-deg", 1);
        m.lonN = m.ai.getNode("position/longitude-deg", 1);
        m.altN = m.ai.getNode("position/altitude-ft", 1);
        m.hdgN = m.ai.getNode("orientation/true-heading-deg", 1);
        m.pitchN = m.ai.getNode("orientation/pitch-deg", 1);
        m.rollN = m.ai.getNode("orientation/roll-deg", 1);
        m.speedN = m.ai.getNode("velocities/speed-kt", 1);
        m.distN = m.ai.getNode("behaviour/distance", 1);

        m.latN.setDoubleValue(m.coord.lat());
        m.lonN.setDoubleValue(m.coord.lon());
        m.altN.setDoubleValue(geodinfo(m.coord.lat(), m.coord.lon())[0] * M2FT);

        #m.update();
        
        m.model.getNode("path", 1).setValue(path);
        m.model.getNode("latitude-deg-prop", 1).setValue(m.latN.getPath());
        m.model.getNode("longitude-deg-prop", 1).setValue(m.lonN.getPath());
        m.model.getNode("elevation-ft-prop", 1).setValue(m.altN.getPath());
        m.model.getNode("heading-deg-prop", 1).setValue(m.hdgN.getPath());
        m.model.getNode("pitch-deg-prop", 1).setValue(m.pitchN.getPath());
        m.model.getNode("roll-deg-prop", 1).setValue(m.rollN.getPath());
        m.model.getNode("load", 1).remove();
        
        return m;

      }
      FollowMe.destruct = func {
        followme_msg("Goodday! " ~ me.callsign ~ " out.");
        me.model.remove();
        me.ai.remove();
      }
      FollowMe.restart =  func {
        print("FollowMe: starting ... brmmm brmmm - meep meep ...");
        var act_apt = airportinfo();
        me.airport.load();

        if (!me.airport.dataIsValid()) {
          print("FollowME: airport file could not be loaded.");
          return;
        }
           
        var parkingkey = me.airport.get_best_parking();
        var startnodekey = me.airport.get_next_exit();

        print("FollowME: escort plane from node ", startnodekey, " to parking node ", parkingkey);
        setprop("/sim/messages/ground", "Clear Runway as soon as possible and taxi via FollowMe to parking " ~ me.airport.nodes[parkingkey].name);
        var startnode = me.as_best_path(startnodekey,parkingkey);
        if (startnode != nil) {
          print("FollowME: set new startnode..");
          me.coord.set(startnode.coord);
          me.latN.setDoubleValue(me.coord.lat());
          me.lonN.setDoubleValue(me.coord.lon());
          me.alt = geodinfo(me.coord.lat(), me.coord.lon())[0] * M2FT;
          me.coord.set_alt(me.alt);
          me.altN.setDoubleValue(me.alt);
          me.nextwpt = startnode.successor;
          me.heading = me.coord.course_to(me.nextwpt.coord);
          me.hdgN.setDoubleValue(me.heading);
        } else {
          print("FollowME: no start node found!");
        }
        if (me.nextwpt != nil)
          me.update();
        else
          print("FollowME: followme did not find the path!");
      }

      FollowMe.update = func() {
        me.dt = getprop("sim/time/delta-sec");
        
        me.coord.set_alt(me.alt * FT2M);
        me.distance = me.coord.direct_distance_to(geo.aircraft_position());
        
        if (me.distance > 80) {
          #aircraft is too far away, keep waiting for aircraft to follow me
          settimer(func me.update(),1);
          #print('too far : ', me.distance, ' alt: ', me.alt);
          return;
        }

        # speed limiter if aircraft cannot catch up        
        if (me.distance < 20) {
          me.coord.apply_course_distance(me.heading, me.speed * me.dt);
        } else {
          me.coord.apply_course_distance(me.heading, me.speed * me.dt * (30/(10+me.distance)));
        }
        
        
        # calculate heading of followme with calculate an accurate radius on each corner  
        me.headingtarget = me.nextwpt==nil?0:me.coord.course_to(me.nextwpt.coord);
        me.headingdiff = (me.headingtarget - me.heading);
        
        me.headingdiff = normdeg(me.headingdiff);

        me.heading = me.heading + me.headingdiff * me.dt;
          
        if ( me.heading > 360 )
          me.heading = me.heading - 360;
        if ( me.heading < 0 )
          me.heading = me.heading + 360;
          
        me.coord.apply_course_distance(me.heading, me.speed * me.dt * (1/(me.distance?me.distance:1)));
        
        me.latN.setDoubleValue(me.coord.lat());
        me.lonN.setDoubleValue(me.coord.lon());
        me.hdgN.setDoubleValue(me.heading);

        me.alt = geodinfo(me.coord.lat(), me.coord.lon())[0] * M2FT;
        me.altN.setDoubleValue(me.alt);

        if (me.nextwpt == nil)
          return;
        
        if (me.coord.direct_distance_to(me.nextwpt.coord) < 10) {
          if (me.nextwpt.successor != nil)
            me.nextwpt = me.nextwpt.successor;
          else
            # stop follow me - last node reached!
            return;
        } 
        
        settimer(func me.update(),0);
        
      }
    
      # as_remove_min: removes node with minimum f value
      FollowMe.as_remove_min = func(list) {
        var minimal = nil;
        foreach (var n; keys(list)) {
          if ((minimal == nil) or (me.f_value[n] and (minimal > me.f_value[n]))) {
            minimal = n;
          }   
        }
        delete(list,minimal);
        return minimal;
      }

      # as_expandnode: expands algo to attached nodes
      FollowMe.as_expandnode = func(current, to) {
        foreach (var succ; me.airport.nodes[current].attached) {
          if (contains(me.closedlist, succ)) {
            continue;
          }
          
          var tentative_g = me.g_value[current]?me.g_value[current]:0 + me.airport.nodes[current].coord.direct_distance_to(me.airport.nodes[succ].coord);
          
          if (contains(me.openlist, succ) and (tentative_g >= me.g_value[succ])) {
            continue;
          }
          
          me.airport.nodes[succ].predecessor = me.airport.nodes[current];
          me.g_value[succ] = tentative_g;
          
          me.f_value[succ] = tentative_g + me.airport.nodes[succ].heuristic(me.airport.nodes[to]);
          me.openlist[succ] = me.airport.nodes[succ];
        }
        return;
      }
      
      # find best path from 'from' to 'to' node
      # returns first node of best path with path as double chained list
      FollowMe.as_best_path = func(from, to) {
        # init lists
        me.openlist = {}; 
        me.closedlist = {};
        me.f_value = {};
        me.g_value = {};
        
        #initialize end 
        me.airport.nodes[from].predecessor = nil;
        me.airport.nodes[to].successor = nil;
        me.openlist[from] = me.airport.nodes[from];
        
        while (size(me.openlist) > 0) {
          var current = me.as_remove_min(me.openlist);

          if (current == to) {
            var n = me.airport.nodes[to];
            var max_iterations = size(me.openlist) + 25;
            while (n.predecessor != nil) {
              n.predecessor.successor = n;
              n = n.predecessor;
              if (max_iterations < 20) {
                print("FollowME: Surplus of iterations. ind: " ~ n.index);
              } else
              if (max_iterations < 1) {
                print("FollowME: max cycles reached! emergency eject!");
                return nil;
              } 
              max_iterations -= 1;
            }
            return me.airport.nodes[from];
          }
        
          me.as_expandnode(current, to);
          
          me.closedlist[current] = me.airport.nodes[from];
        }
        
        return nil;
      }

# ***** node class definition *****
      
      node = {};

      node.new = func(lat,lon, idx) {        
        obj = { parents : [node],
                attached : [],
                successor : nil,
                predecessor : nil };
        obj.index = idx;
        obj.coord = geo.Coord.new();
        obj.coord.set_latlon(lat, lon, 0);
        obj.coord.set_alt(geodinfo(lat, lon)[0]);
        return obj;
      }

      node.addsucc = func(succ) {
        append(me.attached, succ);
      }
      
      node.heuristic = func(node) {
        return me.coord.direct_distance_to(node.coord);
      }

      parking = {};
      
      parking.new = func(lat, lon, hdng, rds, typ, nam, num, idx) {
        obj = { parents : [parking, node.new(lat, lon, idx)] };
        obj.heading = hdng;
        obj.radius = rds;
        obj.type = typ;
        obj.name = nam;
        obj.number = num;
        obj.index = idx;
        return obj;
      }
      
      parking.isBusy = func() {
        var aiplanes = props.globals.getNode("/ai/models");
        var plane = nil;
        var position = nil;
        var coord = geo.Coord.new();
        
        for (var modelnum = 0; 1 ; modelnum += 1)
        {
          if ((plane = aiplanes.getChild("aircraft", modelnum, 0)) != nil)
          {
            position = plane.getNode("position",0);
            if (position.getNode("altitude-ft",0).getValue() > 3000) {
              continue;
            }
            coord.set_latlon(position.getNode("latitude-deg",0).getValue(), position.getNode("longitude-deg",0).getValue(), 0);
            if (coord.direct_distance_to(me.coord) < me.radius) {
              return 1;
            }
          } else {
            return 0;
          }
        }
      }


# ***** end of node class definition *****
      
# ***** model handling *****
 
      var FMinstance = nil;

# ***** end of model handling *****
      
      var wowscanstate = "scanning";
      var wowchangestamp = systime();
      
      var rec_wow_change = func {
        print("FollowME: touched ground.");
        wowchangestamp = systime();
        if (wowscanstate == "scanning") {
          settimer(detect_landing,3);
          wowscanstate = "waiting";
        } 
      }
      
      var detect_landing = func {
        print("FollowME: landing scan...");
        if ((systime() - wowchangestamp) > 2.5 ) {
          setprop("/nasal/followme/landed",getprop("/gear/gear/wow"));
          wowscanstate = "scanning";         
        } else {
          settimer(detect_landing, 2);
          wowscanstate = "waiting";
        }        
      }
      setlistener("/gear/gear/wow", rec_wow_change, 1, 0);
      
      setprop("/nasal/followme/landed", getprop("/gear/gear/wow"));
      setlistener("/nasal/followme/landed", func { if (getprop("/nasal/followme/landed") == "1") { setprop("/sim/messages/ground",getprop("/sim/user/callsign") ~ ", " ~ airportinfo().name ~ " Ground, welcome to " ~ airportinfo().name ~ " airport."); if (FMinstance != nil) FMinstance.restart(); } }, 0, 0);
      
      _setlistener("/sim/sceneryloaded", func { print("FollowME: init"); if (getprop("/sim/sceneryloaded")) { print("FollowME: creating instance"); FMinstance = FollowMe.new(FOLLOWMEmodel); }}, 1, 0 );

