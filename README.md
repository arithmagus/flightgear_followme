# flightgear_followme

**FollowMe Car for FlightGear**

This is an autonomous followme car for flightgear, that guides you after landing from the runway to a suitable parking space.

The car makes use of the airport nodes, read from the airport scenery xml, looks for a suitable parking space and calculates the best way for guiding your plane there.

Just copy these files into your fg_data directory, and enable it in your fg_data\defaults.xml within "//PropertyList/nasal" of your defaults.xml.

```
    <followme>
      <enabled type="bool">true</enabled>
    </followme>
```

References

This followme car is based on a 3D model from Robert Leda/David Glowski "hatchback_red" from flightgear repository.


License

This followme car is published under GPLv3 License.

